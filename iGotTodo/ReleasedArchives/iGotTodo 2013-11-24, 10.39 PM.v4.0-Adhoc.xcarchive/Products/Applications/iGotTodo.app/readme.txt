16-Nov: Implementing state restoration
* https://developer.apple.com/library/ios/documentation/iphone/conceptual/iphoneosprogrammingguide/StatePreservation/StatePreservation.html
* http://www.techotopia.com/index.php/An_iOS_6_iPhone_State_Preservation_and_Restoration_Tutorial
* http://ideveloper.co/introduction-to-ui-state-preservation-restoration-in-ios6/ (testing method - stop from xcode)
* http://www.slideshare.net/robby_brown/ios-state-preservation-and-restoration

16-Nov: XIB implementation reference
* http://onedayitwillmake.com/blog/2013/07/ios-creating-reusable-uiviews-with-storyboard/

17-Nov: Swipe
* http://idevrecipes.com/2011/04/14/how-does-the-twitter-iphone-app-implement-side-swiping-on-a-table/